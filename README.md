# Livres

* [Le jardinier maraîcher, JM Fortier](http://www.boitealivres.com/9782897192044-le-jardinier-maraicher-manuel-d-agriculture-biologique-sur-petite-surface-jean-martin-fortier/)
* [Maraîchage biologique : la méthode d'Eliot Coleman ; techniques et outils de culture](http://www.boitealivres.com/9782330132668-maraichage-biologique-la-methode-d-eliot-coleman-techniques-et-outils-de-culture-eliot-coleman/)
* [Le guide du jardin bio, JP Thorez](http://www.boitealivres.com/9782360985616-le-guide-du-jardin-bio-jean-paul-thorez/)
* [Manuel de Culture maraîchère, Moreau et Daverne](https://github.com/permacole/culture-maraichere/)
* [Coccinelles primèveres mésanges..., D Pépin](http://www.boitealivres.com/9782914717410-coccinelles-primeveres-mesanges-denis-pepin/)
* [Le sol, la terre et les champs ; pour retrouver une agriculture saine, C et L Bourguignon](http://www.boitealivres.com/9782869853263-le-sol-la-terre-et-les-champs-pour-retrouver-une-agriculture-saine-claude-bourguignon-lydia-bourguignon/)

# Planification

[Logiciel de planification des cultures](https://qrop.frama.io/)

# Repères

* [Guide : je m'installe en maraîchage biologique en région Centre - Val de Loire](https://www.bio-centre.org/index.php/produire-bio/developper-mon-activite-bio/maraichage/18-etre-accompagne-par-le-reseau-bio/172-guide-je-m-installe-en-maraichage-bio-en-region-centre-val-de-loire)

* [Maraîchage bio en basse Normandie : des clés pour se repérer](https://produirebio-normandie.org/wp-content/uploads/2017/01/LIVRET-MARAICHAGE-BN-WEB.pdf) et les [Fiches des fermes](https://produirebio-normandie.org/wp-content/uploads/2017/12/Fiches-de-fermes-Trajectoires-WEB.pdf)

* [Les publications de Bio Centre](https://www.bio-centre.org/index.php/les-publications-de-bio-centre)

# Fiches techniques

* [Fiches techniques de production biologique](https://www.grab.fr/fiches-techniques-de-production-biologique/)
* [Les auxiliaires et le maraîchage, Guide technique](https://pays-de-la-loire.chambres-agriculture.fr/fileadmin/user_upload/National/FAL_commun/publications/Pays_de_la_Loire/depliant_12_auxiliaires_maraichage_01.pdf)

